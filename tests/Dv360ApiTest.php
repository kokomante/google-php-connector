<?php

use DMS\PHPUnitExtensions\ArraySubset\ArraySubsetAsserts;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Kokomante\GoogleConnector\Api\Dv360Api;
use Kokomante\GoogleConnector\Auth\GoogleOAuth2;
use Kokomante\GoogleConnector\Exceptions\Dv360ApiException;
use PHPUnit\Framework\TestCase;

class Dv360ApiTest extends TestCase
{
    use ArraySubsetAsserts;

    private Dv360Api $bigQueryApi;
    private MockHandler $mock;

    private function setDv360Api()
    {
        $this->mock = new MockHandler([
            new Response(200, ['X-Foo' => 'Bar'], json_encode([ 'test' => '123456' ]))
        ]);
        $handlerStack = HandlerStack::create($this->mock);
        $client = new Client(['handler' => $handlerStack]);

        $googleAuth = $this->createMock(GoogleOAuth2::class);
        $googleAuth->method('getAccessToken')->willReturn('123456');


        $this->bigQueryApi = new Dv360Api($googleAuth, $client, 'projectId');
    }

    public function testDv360ApiFromConfig()
    {
        $bigQueryApi = Dv360Api::fromConfig([
            'clientId' => '123456',
            'clientSecret' => '123456',
            'refreshToken' => '123456',
        ]);

        $this->assertInstanceOf(Dv360Api::class, $bigQueryApi);
    }

    public function testDv360ApiThrowExceptionIfClientIdNotSet()
    {
        $this->expectException(Dv360ApiException::class);
        Dv360Api::fromConfig([
            'clientSecret' => '123456',
            'refreshToken' => '123456',
            'projectId' => 'projectId',
        ]);
    }

    public function testDv360ApiThrowExceptionIfClientSecretNotSet()
    {
        $this->expectException(Dv360ApiException::class);
        Dv360Api::fromConfig([
            'clientId' => '123456',
            'refreshToken' => '123456',
            'projectId' => 'projectId',
        ]);
    }

    public function testDv360ApiThrowExceptionIfRefreshTokenNotSet()
    {
        $this->expectException(Dv360ApiException::class);
        Dv360Api::fromConfig([
            'clientId' => '123456',
            'clientSecret' => '123456',
            'projectId' => 'projectId'
        ]);
    }

    public function testDv360ApiConfigIsNotAnArray()
    {
        $this->expectException(Dv360ApiException::class);
        Dv360Api::fromConfig('test');
    }
}