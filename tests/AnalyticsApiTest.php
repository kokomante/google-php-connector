<?php

use DMS\PHPUnitExtensions\ArraySubset\ArraySubsetAsserts;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Kokomante\GoogleConnector\Auth\GoogleOAuth2;
use PHPUnit\Framework\TestCase;
use Kokomante\GoogleConnector\Api\AnalyticsApi;
use Kokomante\GoogleConnector\Exceptions\AnalyticsApiException;

class AnalyticsApiTest extends TestCase
{
    use ArraySubsetAsserts;

    /* @var AnalyticsApi */
    private AnalyticsApi $analyticsApi;


    /**
     * @var MockHandler
     */
    private MockHandler $mock;

    private function setAnalyticsApi()
    {
        $this->mock = new MockHandler([
            new Response(200, ['X-Foo' => 'Bar'], json_encode(['test' => '123456']))
        ]);
        $handlerStack = HandlerStack::create($this->mock);
        $client = new Client(['handler' => $handlerStack]);

        $googleAuth = $this->createMock(\Kokomante\GoogleConnector\Auth\GoogleOAuth2::class);
        $googleAuth->method('getAccessToken')->willReturn('123456');


        $this->analyticsApi = new AnalyticsApi($googleAuth, $client);

    }

    public function testReportRequestsExceptionWhenClientStatusNotOk()
    {
        $mock = new MockHandler([
            new Response(500, ['X-Foo' => 'Bar'], json_encode(['error' => 'error']))
        ]);

        $handlerStack = HandlerStack::create($mock);

        $client = new Client(['handler' => $handlerStack]);

        $googleAuth = $this->createMock(GoogleOAuth2::class);
        $googleAuth->method('getAccessToken')->willReturn('123456');


        $analyticsApi = new AnalyticsApi($googleAuth, $client);
        $this->expectException(AnalyticsApiException::class);
        $analyticsApi->reportRequests("query");

    }

    public function testClientConfigReportRequests()
    {
        $this->setAnalyticsApi();
        $this->analyticsApi->reportRequests(['reports' => 'example']);
        $this->assertArraySubset([
            'Authorization' => ['Bearer 123456']
        ], $this->mock->getLastRequest()->getHeaders(), true);

        $this->assertEquals('https://analyticsreporting.googleapis.com/v4/reports:batchGet',
            $this->mock->getLastRequest()->getUri());

        $this->assertEquals("POST", $this->mock->getLastRequest()->getMethod());

        $this->assertEquals(json_encode(['reportRequests' => ['reports' => 'example']]), $this->mock->getLastRequest()->getBody());
    }

    public function testReportRequests()
    {
        $this->setAnalyticsApi();
        $response = $this->analyticsApi->reportRequests(['reports' => 'example']);
        $this->assertEquals(['test' => '123456'], $response);
    }

    public function testRealTimeExceptionWhenClientStatusNotOk()
    {
        $mock = new MockHandler([
            new Response(500, ['X-Foo' => 'Bar'], json_encode(['error' => 'error']))
        ]);

        $handlerStack = HandlerStack::create($mock);

        $client = new Client(['handler' => $handlerStack]);

        $googleAuth = $this->createMock(GoogleOAuth2::class);
        $googleAuth->method('getAccessToken')->willReturn('123456');


        $analyticsApi = new AnalyticsApi($googleAuth, $client);
        $this->expectException(AnalyticsApiException::class);
        $analyticsApi->realTime("query");

    }

    public function testClientConfigRealTime()
    {
        $this->setAnalyticsApi();
        $this->analyticsApi->realTime(['reports' => 'example']);
        $this->assertArraySubset([
            'Authorization' => ['Bearer 123456']
        ], $this->mock->getLastRequest()->getHeaders(), true);

        $this->assertEquals("GET", $this->mock->getLastRequest()->getMethod());

        $this->assertEquals('https://www.googleapis.com/analytics/v3/data/realtime?reports=example',
            $this->mock->getLastRequest()->getUri());
    }

    public function testRealTime()
    {
        $this->setAnalyticsApi();
        $response = $this->analyticsApi->realTime(['reports' => 'example']);
        $this->assertEquals(['test' => '123456'], $response);
    }

    public function testAnalyticsApiFromConfig()
    {
        $analyticsApi = AnalyticsApi::fromConfig([
            'clientId' => '123456',
            'clientSecret' => '123456',
            'refreshToken' => '123456'
        ]);

        $this->assertInstanceOf(AnalyticsApi::class, $analyticsApi);
    }

    public function testAnalyticsApiThrowExceptionIfClientIdNotSet()
    {
        $this->expectException(AnalyticsApiException::class);
        AnalyticsApi::fromConfig([
            'clientSecret' => '123456',
            'refreshToken' => '123456',
        ]);
    }

    public function testAnalyticsApiThrowExceptionIfClientSecretNotSet()
    {
        $this->expectException(AnalyticsApiException::class);
        AnalyticsApi::fromConfig([
            'clientId' => '123456',
            'refreshToken' => '123456'
        ]);
    }

    public function testAnalyticsApiThrowExceptionIfRefreshTokenNotSet()
    {
        $this->expectException(AnalyticsApiException::class);
        AnalyticsApi::fromConfig([
            'clientId' => '123456',
            'clientSecret' => '123456'
        ]);
    }


    public function testAnalyticsApiConfigIsNotAnArray()
    {
        $this->expectException(AnalyticsApiException::class);
        AnalyticsApi::fromConfig('test');
    }
}