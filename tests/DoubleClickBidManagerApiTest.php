<?php

use Carbon\Carbon;
use DMS\PHPUnitExtensions\ArraySubset\ArraySubsetAsserts;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Kokomante\GoogleConnector\Api\DoubleClickBidManagerApi;
use Kokomante\GoogleConnector\Auth\GoogleOAuth2;
use Kokomante\GoogleConnector\Exceptions\DoubleClickBidManagerApiException;
use PHPUnit\Framework\TestCase;

class DoubleClickBidManagerApiTest extends TestCase
{
    use ArraySubsetAsserts;

    private DoubleClickBidManagerApi $bigQueryApi;
    private MockHandler $mock;

    private function setDoubleClickBidManagerApi()
    {
        $this->mock = new MockHandler([
            new Response(200, ['X-Foo' => 'Bar'], json_encode([ 'test' => '123456' ]))
        ]);
        $handlerStack = HandlerStack::create($this->mock);
        $client = new Client(['handler' => $handlerStack]);

        $googleAuth = $this->createMock(GoogleOAuth2::class);
        $googleAuth->method('getAccessToken')->willReturn('123456');


        $this->bigQueryApi = new DoubleClickBidManagerApi($googleAuth, $client, 'projectId');
    }

    public function testDoubleClickBidManagerApiFromConfig()
    {
        $bigQueryApi = DoubleClickBidManagerApi::fromConfig([
            'clientId' => '123456',
            'clientSecret' => '123456',
            'refreshToken' => '123456',
        ]);

        $this->assertInstanceOf(DoubleClickBidManagerApi::class, $bigQueryApi);
    }

    public function testDoubleClickBidManagerApiThrowExceptionIfClientIdNotSet()
    {
        $this->expectException(DoubleClickBidManagerApiException::class);
        DoubleClickBidManagerApi::fromConfig([
            'clientSecret' => '123456',
            'refreshToken' => '123456',
            'projectId' => 'projectId',
        ]);
    }

    public function testDoubleClickBidManagerApiThrowExceptionIfClientSecretNotSet()
    {
        $this->expectException(DoubleClickBidManagerApiException::class);
        DoubleClickBidManagerApi::fromConfig([
            'clientId' => '123456',
            'refreshToken' => '123456',
            'projectId' => 'projectId',
        ]);
    }

    public function testDoubleClickBidManagerApiThrowExceptionIfRefreshTokenNotSet()
    {
        $this->expectException(DoubleClickBidManagerApiException::class);
        DoubleClickBidManagerApi::fromConfig([
            'clientId' => '123456',
            'clientSecret' => '123456',
            'projectId' => 'projectId'
        ]);
    }

    public function testDoubleClickBidManagerApiConfigIsNotAnArray()
    {
        $this->expectException(DoubleClickBidManagerApiException::class);
        DoubleClickBidManagerApi::fromConfig('test');
    }
}