<?php

use DMS\PHPUnitExtensions\ArraySubset\ArraySubsetAsserts;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Kokomante\GoogleConnector\Api\BigQueryApi;
use Kokomante\GoogleConnector\Auth\GoogleOAuth2;
use Kokomante\GoogleConnector\Exceptions\BigQueryApiException;
use PHPUnit\Framework\TestCase;

class BigQueryApiTest extends TestCase
{
    use ArraySubsetAsserts;

    private BigQueryApi $bigQueryApi;
    private MockHandler $mock;

    private function setBigQueryApi()
    {
        $this->mock = new MockHandler([
            new Response(200, ['X-Foo' => 'Bar'], json_encode([ 'test' => '123456' ]))
        ]);
        $handlerStack = HandlerStack::create($this->mock);
        $client = new Client(['handler' => $handlerStack]);

        $googleAuth = $this->createMock(GoogleOAuth2::class);
        $googleAuth->method('getAccessToken')->willReturn('123456');


        $this->bigQueryApi = new BigQueryApi($googleAuth, $client, 'projectId');
    }

    public function testBigQueryApiFromConfig()
    {
        $bigQueryApi = BigQueryApi::fromConfig([
            'clientId' => '123456',
            'clientSecret' => '123456',
            'refreshToken' => '123456',
            'projectId' => 'projectId'
        ]);

        $this->assertInstanceOf(BigQueryApi::class, $bigQueryApi);
    }

    public function testBigQueryApiThrowExceptionIfClientIdNotSet()
    {
        $this->expectException(BigQueryApiException::class);
        BigQueryApi::fromConfig([
            'clientSecret' => '123456',
            'refreshToken' => '123456',
            'projectId' => 'projectId',
        ]);
    }

    public function testBigQueryApiThrowExceptionIfClientSecretNotSet()
    {
        $this->expectException(BigQueryApiException::class);
        BigQueryApi::fromConfig([
            'clientId' => '123456',
            'refreshToken' => '123456',
            'projectId' => 'projectId',
        ]);
    }

    public function testBigQueryApiThrowExceptionIfRefreshTokenNotSet()
    {
        $this->expectException(BigQueryApiException::class);
        BigQueryApi::fromConfig([
            'clientId' => '123456',
            'clientSecret' => '123456',
            'projectId' => 'projectId'
        ]);
    }

    public function testBigQueryApiThrowExceptionIfProjectIdNotSet()
    {
        $this->expectException(BigQueryApiException::class);
        BigQueryApi::fromConfig([
            'clientId' => '123456',
            'clientSecret' => '123456',
            'refreshToken' => '123456',
        ]);
    }

    public function testBigQueryApiConfigIsNotAnArray()
    {
        $this->expectException(BigQueryApiException::class);
        BigQueryApi::fromConfig('test');
    }
}