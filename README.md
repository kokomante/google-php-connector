# Kokomante Google Api Connector  

This project hosts the PHP Google Api Connector for the various Google APIs (Ads, YouTube, Analytics...)  

[[_TOC_]]

## Install  
  
### Using composer require  
  
The library will be downloaded by Composer and stored under the  `vendor/` directory.  **Examples are NOT downloaded by this download method.**  
1. Add this repository on your composer.json

    ```  
	"repositories": [ { "type": "composer", "url": "https://packages.zeus.vision/" }],
	```
2. Install the latest version using  [Composer](https://getcomposer.org/).  
      
	```  
	composer require zeus/google-connector-php  
	```
2. Follow  [Using OAuth 2.0 to Access Google APIs](https://developers.google.com/identity/protocols/oauth2)  if you haven't set up the credentials yet.  
      
3. You can now use the library.  
  
## Analytics  
  
AnalyticsApi allows us to access Google Analytics Report v4 and Real Time v3.
  
### Setting up your credentials  
  
1. Get a OAuth2 **Client ID** and **Secret** from Credentials section over your [Google Cloud Platform](https://console.cloud.google.com/apis/credentials)  
2. Get a **Refresh Token** calling our refresh-token.php script from your root project directory, which will prompt you for your OAuth2 client ID and secret.  
	```console
	php vendor/bin/refresh-token.php  
	```  
  
### Basic Usage  
  
To get a report with Report Api v4 create an instance of AnalyticsApi with your config and send the reports requests array from the **reportRequests** method:  
  
```php 
  
use \Kokomante\GoogleConnector\Api\AnalyticsApi;  
  
$analyticsApi = AnalyticsApi::fromConfig([  
  'clientId' => 'xxx',  
  'clientSecret' => 'xxx',
  'refreshToken' => 'xxx',  
]);  

$response = $analyticsApi->reportRequests([  
  'viewId' => '12345678',  
  'dateRanges' => [  
 [  'startDate' => '2020-10-01',  
  'endDate' => '2020-10-08'  
  ]  
 ],  
  'metrics' => [  
 ['expression' => 'ga:sessions']  
 ],  
  'dimensions' => [  
 ['name' => 'ga:browser']  
 ]]);
```  
  
Learn about ReportRequests in [Analytics Dev Guides](https://developers.google.com/analytics/devguides/reporting/core/v4/rest/v4/reports/batchGet?authuser=3#reportrequest)   

To get a real time metrics with RealTime Api v3 create an instance of AnalyticsApi with your config and send the param queries array from the **realTime** method:  
  
```php   
use \Kokomante\GoogleConnector\Api\AnalyticsApi;  
  
$analyticsApi = AnalyticsApi::fromConfig([  
  'clientId' => 'xxx',  
  'clientSecret' => 'xxx',
  'refreshToken' => 'xxx',  
]);  

$response = $analyticsApi->realTime([  
  'ids' => 'ga:95737033',  
  'metrics' => 'rt:activeUsers'  
]);
```  
  
Learn about param queries in [Analytics Dev Guides](https://developers.google.com/analytics/devguides/reporting/realtime/v3/reference/data/realtime/get?authuser=3#parameters)  
  
## AdsApi

AdsApi allows us to access Google Ads reports through the [Google Ads Query Language.](https://developers.google.com/google-ads/api/docs/query/overview)   
  
### Setting up your credentials  
  
1. Get a OAuth2 **Client ID** and **Secret** from Credentials section over your [Google Cloud Platform](https://console.cloud.google.com/apis/credentials)  
2. Get a **Refresh Token** calling our refresh-token.php script from your root project directory, which will prompt you for your OAuth2 client ID and secret.  
	```console
	php vendor/bin/refresh-token.php  
	```  
3. Get **Development Token** from your [Google Ads API Center](https://ads.google.com/aw/apicenter)  
4. Get your **Customer ID** (account id which you are consulting) and your **Login Customer ID** (parent account id) from your Google Ads Console. [Find your Customer ID](https://support.google.com/google-ads/answer/1704344?hl=en)  
> **Remove hyphens from your customerId and loginCustomerId:** xxx-XXX-xxx  to xxxXXXxxx  
  
### Basic Usage  
  
To get a report with Google Ads Query Language, create an instance of AdsApi with your config and send the query from the **searchStream** method:  
  
```php 
use Kokomante\GoogleConnector\Api\AdsApi;  
  
$adsApi = AdsApi::fromConfig([    
    'clientId' => $config['clientId'],    
    'clientSecret' => $config['clientSecret'],    
    'refreshToken' => $config['refreshToken'],    
    'developerToken' => $config['developerToken'],    
    'customerId' => $config['customerId'],    
'loginCustomerId' => $config['loginCustomerId'] ]);  
  
$reportObject = $adsApi->searchStream("  
 SELECT  campaign.id, campaign.name,  campaign.status FROM campaign    ORDER BY campaign.id  
");  
```  
  
Learn all reports query options in [Google Developers Ads Api Page](https://developers.google.com/google-ads/api/docs/reporting/example)   

## BigQuery

BigQuery allows us to access [Google Big Query Rest Api.](https://cloud.google.com/bigquery/docs/reference/rest)

### Setting up your credentials

1. Get a OAuth2 **Client ID** and **Secret** from Credentials section over your [Google Cloud Platform](https://console.cloud.google.com/apis/credentials)
2. Get a **Refresh Token** calling our refresh-token.php script from your root project directory, which will prompt you for your OAuth2 client ID and secret.
   ```console
   php vendor/bin/refresh-token.php  
   ```  
   
### Basic Usage

1. First, create an instance of Big Query Api providing the oauth2 credentials and the ID of your Google Big Query Project.
2. [Optional] Pass the Api version that you want to use. If the api version is not passed, it will use the v2 api. 

```php 
use Kokomante\GoogleConnector\Api\BigQueryApi;  
  
$bigQueryApi = BigQueryApi::fromConfig([  
  'clientId' => 'xxxxx-xxx.apps.googleusercontent.com',  
  'clientSecret' => 'xxxxxxxxxxxxxxxxxxxxxxxx',
  'refreshToken' => 'xxxxxxxxxxxxxxxxxxxxxxxx',  
  'projectId' => 'projectName',
  'version' => 'v2',
]);  
```  
Now, you can create a request by passing a mySQL query and letting the function handle for you this actions: 
1. Create a Job with your query
2. Run the Job and wait for the response
3. Handle the pagination
4. Parse the response to an array.

```php 
$response = $bigQueryApi->request($query)
```  
Or take control over this steps by running the needed functions.
```php 
$job = $bigQueryApi->createJob($query);
$jobResponse = $bigQueryApi->runJob($job);
$resultArray = $bigQueryApi->parseResponse($jobResponse);
```  
Additionally, you can get the Dataset info with the getDataset method.
```php 
$datasetId = 'dataset_name';
$dataset = $bigQueryApi->getDataset($datasetId);
```  

## Dv360

Dv360 allows us to access [Google Dv360 Api.](https://developers.google.com/display-video/api/reference/rest) This Api is commonly used with the [Double Click Bid Manager Api](#double-click-bid-manager) described in the next section.

Dv360 Api allows you to retrieve campaigns, insertion orders, line items, creatives... of advertisers within a partner. This Api don't let you retrieve the insights data, for this purpose you must use the [Double Click Bid Manager Api](#double-click-bid-manager) 

### Setting up your credentials

1. Get a OAuth2 **Client ID** and **Secret** from Credentials section over your [Google Cloud Platform](https://console.cloud.google.com/apis/credentials)
2. Get a **Refresh Token** calling our refresh-token.php script from your root project directory, which will prompt you for your OAuth2 client ID and secret.
   ```console
   php vendor/bin/refresh-token.php  
   ```  

### Basic Usage
1. First, create an instance of Dv360 Api providing the oauth2 credentials.
2. [Optional] Pass the Api version that you want to use. If the api version is not passed, it will use the v1 api.

```php 
use Kokomante\GoogleConnector\Api\Dv360Api;  
  
$dv360Api = Dv360Api::fromConfig([  
  'clientId' => 'xxxxx-xxx.apps.googleusercontent.com',  
  'clientSecret' => 'xxxxxxxxxxxxxxxxxxxxxxxx',
  'refreshToken' => 'xxxxxxxxxxxxxxxxxxxxxxxx',  
  'version' => 'v1',
]);  
```  
You can make a custom request passing the method, endpoint and params if needed.
```php 
$dv360Api->makeRequest(string $method, string $endpoint, $params = []);
```  
Or use one of the predefined methods:
```php 
// Get the partner data
$dv360Api->getPartner(int $partnerId);

// Get the advertiser data
$dv360Api->getAdvertiser(int $advertiserId);

// Get the advertiser campaigns
$dv360Api->getCampaigns(int $advertiserId);

// Get the advertiser insertion orders
$dv360Api->getInsertionOrders(int $advertiserId);

// Get the advertiser line items
$dv360Api->getLineItems(int $advertiserId);

// Get the advertiser creative data
$dv360Api->getCreative(int $advertiserId, int $creativeId);
```  

## Double Click Bid Manager

Double Click Bid Manager allows us to access [Google Double Click Bid Manager Api.](https://developers.google.com/bid-manager/v1.1) This Api is commonly used with the [Dv360 Api](#dv360) described in the previous section.

Double Click Bid Manager Api allows you to generate insights reports, download generated reports(*), and delete reports.

(*) To make easier manage the reports, this document describes a way to get the download csv report as an array with the **getReport** method. This method is explained later.

### Basic Usage
1. First, create an instance of Dv360 Api providing the oauth2 credentials.
2. [Optional] Pass the Api version that you want to use. If the api version is not passed, it will use the v1.1 api.

```php 
use Kokomante\GoogleConnector\Api\DoubleClickBidManagerApi;  
  
$doubleClickBidManagerApi = DoubleClickBidManagerApi::fromConfig([  
  'clientId' => 'xxxxx-xxx.apps.googleusercontent.com',  
  'clientSecret' => 'xxxxxxxxxxxxxxxxxxxxxxxx',
  'refreshToken' => 'xxxxxxxxxxxxxxxxxxxxxxxx',  
  'version' => 'v1.1',
]);  
```  

You can make a custom request passing the method, endpoint and params if needed.

```php 
$doubleClickBidManagerApi->makeRequest(string $method, string $endpoint, $params = []);
```

Or use one of the predefined methods:

```php 
// Create a report with the given params
$doubleClickBidManagerApi->createQuery(array $params)

// Get the query metadata including the url to download the csv file
$doubleClickBidManagerApi->getQueryMetadata(int $queryId)

// Get the query metadata, download the csv file, parse and return the response
$doubleClickBidManagerApi->getReport(int $queryId);

// Delete a report
$doubleClickBidManagerApi->deleteQuery(int $queryId)
```  

Example of a createQuery method with parameters. 

```php 
// Create a report
$doubleClickBidManagerApi->createQuery([
            "kind" => "doubleclickbidmanager#query",
            "queryId" => "111406597",
            "metadata" => [
                "title" => "A name for the query",
                "dataRange" => "CUSTOM_DATES",
                "format" => "CSV",
                "running" => true,
                "sendNotification" => false
            ],
            "params" => [
                "type" => "TYPE_GENERAL",
                "groupBys" => [
                    "FILTER_MEDIA_PLAN",
                    "FILTER_DATE",
                    "FILTER_PARTNER_CURRENCY",
                    "FILTER_CREATIVE_SOURCE",
                ],
                "filters" => [
                    [
                        "type" => "FILTER_ADVERTISER",
                        "value" => XXXXXXXXX
                    ],
                ],
                "metrics" => [
                    "METRIC_CLICKS",
                    "METRIC_TOTAL_CONVERSIONS",
                    "METRIC_CONVERSIONS_PER_MILLE",
                    "METRIC_IMPRESSIONS",
                    "METRIC_CTR",
                    "METRIC_REVENUE_PARTNER",
                    "METRIC_TOTAL_MEDIA_COST_ECPM_PARTNER",
                    "METRIC_REVENUE_ECPC_PARTNER",
                    "METRIC_TOTAL_CONVERSIONS",
                    "METRIC_REVENUE_ECPA_PARTNER",
                    "METRIC_MEDIA_COST_ECPCV_PARTNER",
                    "METRIC_RICH_MEDIA_VIDEO_FIRST_QUARTILE_COMPLETES",
                    "METRIC_RICH_MEDIA_VIDEO_MIDPOINTS",
                    "METRIC_RICH_MEDIA_VIDEO_THIRD_QUARTILE_COMPLETES",
                    "METRIC_RICH_MEDIA_VIDEO_COMPLETIONS",
                    "METRIC_RICH_MEDIA_VIDEO_PLAYS",
                ],
            ],
            "schedule" => [
                "frequency" => "ONE_TIME"
            ],
            "reportDataStartTimeMs" => 1634312253000,
            "reportDataEndTimeMs" => 1634826399000,
            "timezoneCode" => "Europe/Copenhagen"
        ]);
```  

## Next modules soon...