<?php


namespace Kokomante\GoogleConnector\Api;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use JetBrains\PhpStorm\ArrayShape;
use Kokomante\GoogleConnector\Auth\GoogleOAuth2;
use Kokomante\GoogleConnector\Auth\GoogleScopes;
use Kokomante\GoogleConnector\Exceptions\AnalyticsApiException;

class AnalyticsApi
{

    private GoogleOAuth2 $googleOAuth2;
    private Client $client;

    const REPORT_REQUEST_URL = "https://analyticsreporting.googleapis.com/v4/reports:batchGet";
    const REALTIME_URL = "https://www.googleapis.com/analytics/v3/data/realtime";


    public function __construct(GoogleOAuth2 $googleOAuth2, Client $client)
    {
        $this->googleOAuth2 = $googleOAuth2;
        $this->client = $client;
    }

    public function reportRequests($reports)
    {
        try {
            $response = $this->client->request('POST', self::REPORT_REQUEST_URL, [
                'headers' => $this->getHeaders(),
                'body' => json_encode(['reportRequests' => $reports])
            ]);

        } catch (GuzzleException $exception) {
            throw new AnalyticsApiException("AnalyticsApi Error. Result: " . (string)$exception->getMessage());
        }

        return json_decode((string)$response->getBody(), true);
    }

    public function realTime($params)
    {
        try {
            $response = $this->client->request('GET', self::REALTIME_URL, [
                'headers' => $this->getHeaders(),
                'query' => $params
            ]);

        } catch (GuzzleException $exception) {
            throw new AnalyticsApiException("AnalyticsApi Error. Result: " . (string)$exception->getMessage());
        }

        return json_decode((string)$response->getBody(), true);
    }

    #[ArrayShape(['Authorization' => "string"])]
    private function getHeaders(): array
    {
        return [
            'Authorization' => 'Bearer ' . $this->googleOAuth2->getAccessToken()
        ];
    }

    public static function fromConfig($config): AnalyticsApi
    {
        self::checkConfig($config);

        $googleOauth2 = new GoogleOAuth2(
            $config['clientId'],
            $config['clientSecret'],
            [GoogleScopes::ANALYTICS_READ_ONLY_SCOPE],
            $config['refreshToken']
        );

        return new self($googleOauth2, new Client());
    }

    private static function checkConfig($config)
    {
        if (!is_array($config)) {
            throw new AnalyticsApiException("Analytics config is not an array");
        }

        $configKeys = ['clientId', 'clientSecret', 'refreshToken'];

        foreach ($configKeys as $key) {
            if (!key_exists($key, $config) || !$config[$key]) {
                throw new AnalyticsApiException("{$key} not set in AnalyticsApi config");
            }
        }
    }
}