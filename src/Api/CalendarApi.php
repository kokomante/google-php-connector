<?php


namespace Kokomante\GoogleConnector\Api;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Kokomante\GoogleConnector\Auth\GoogleOAuth2;
use Kokomante\GoogleConnector\Auth\GoogleScopes;
use Kokomante\GoogleConnector\Exceptions\SheetsApiException;

class CalendarApi
{

    private GoogleOAuth2 $googleOAuth2;
    private Client $client;

    const BASE_URL = "https://www.googleapis.com/calendar/v3";
    const EVENT_LIST = self::BASE_URL . "/calendars/%s/events";


    public function __construct(GoogleOAuth2 $googleOAuth2, Client $client)
    {
        $this->googleOAuth2 = $googleOAuth2;
        $this->client = $client;
    }

    public function getCalendarEventsPaginated($calendarId, $query = array())
    {
        $query['maxResults'] = $query['maxResults'] ?? 500;
        $url = sprintf(self::EVENT_LIST, $calendarId);
        $nextPageToken = null;
        do {
            if ($nextPageToken) {
                $query['pageToken'] = $nextPageToken;
            }
            try {
                $response = $this->client->request('GET', $url, [
                        'headers' => $this->getHeaders(),
                        'query' => $query
                    ]
                );

            } catch (GuzzleException $exception) {
                throw new SheetsApiException("SheetsApi Error. Result: " . (string)$exception->getMessage());
            }

            $result = json_decode((string)$response->getBody(), true);
            yield $result;
            $nextPageToken = isset($result['nextPageToken']) ? $result['nextPageToken'] : null;
        } while ($nextPageToken);
    }

    private function getHeaders()
    {
        return [
            'Authorization' => 'Bearer ' . $this->googleOAuth2->getAccessToken()
        ];
    }

    public static function fromConfig($config)
    {
        self::checkConfig($config);

        $googleOauth2 = new GoogleOAuth2(
            $config['clientId'],
            $config['clientSecret'],
            [GoogleScopes::CALENDAR_READ_ONLY_SCOPE],
            $config['refreshToken']
        );

        return new self($googleOauth2, new Client());
    }

    private static function checkConfig($config)
    {
        if (!is_array($config)) {
            throw new SheetsApiException("Sheets config is not an array");
        }

        $configKeys = ['clientId', 'clientSecret', 'refreshToken'];

        foreach ($configKeys as $key) {
            if (!key_exists($key, $config) || !$config[$key]) {
                throw new SheetsApiException("{$key} not set in SheetsApi config");
            }
        }
    }
}