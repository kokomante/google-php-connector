<?php


namespace Kokomante\GoogleConnector\Api;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Kokomante\GoogleConnector\Auth\GoogleOAuth2;
use Kokomante\GoogleConnector\Auth\GoogleScopes;
use Kokomante\GoogleConnector\Exceptions\BigQueryApiException;
use Kokomante\GoogleConnector\Exceptions\GoogleOauthException;

class BigQueryApi
{
    const API_URL = "https://bigquery.googleapis.com/bigquery/";
    const DEFAULT_VERSION = 'v2';

    private GoogleOAuth2 $googleOAuth2;
    private Client $client;
    private string $projectId;
    private string $path;
    private string $jobsPath;

    public function __construct(GoogleOAuth2 $googleOAuth2, Client $client, $projectId, $apiVersion)
    {
        $this->googleOAuth2 = $googleOAuth2;
        $this->client = $client;
        $this->projectId = $projectId;
        $this->path = self::API_URL . $apiVersion . '/projects/';
        $this->jobsPath = $this->path . $this->projectId . '/jobs';
    }

    /**
     * @throws GoogleOauthException
     */
    private function getHeaders(): array
    {
        return [
            'Authorization' => 'Bearer ' . $this->googleOAuth2->getAccessToken()
        ];
    }


    /**
     * @throws BigQueryApiException
     */
    public static function fromConfig($config): BigQueryApi
    {
        self::checkConfig($config);

        $googleOauth2 = new GoogleOAuth2(
            $config['clientId'],
            $config['clientSecret'],
            [GoogleScopes::BIGQUERY_READ_ONLY_SCOPE],
            $config['refreshToken']
        );

        $apiVersion = $config['version'] ?? self::DEFAULT_VERSION;

        return new self($googleOauth2, new Client(), $config['projectId'], $apiVersion);
    }

    /**
     * @throws BigQueryApiException
     */
    private static function checkConfig($config)
    {
        if (!is_array($config)) {
            throw new BigQueryApiException("BigQuery config is not an array");
        }

        $configKeys = ['clientId', 'clientSecret', 'refreshToken', 'projectId'];

        foreach ($configKeys as $key) {
            if (!key_exists($key, $config) || !$config[$key]) {
                throw new BigQueryApiException($key . " not set in BigQueryApi config");
            }
        }
    }

    /**
     * @throws BigQueryApiException|GoogleOauthException
     */
    public function request($query): array
    {
        $response = [];
        $pageToken = null;
        try {
            $job = $this->createJob($query);
            do {
                $queryResults = $this->runJob($job, $pageToken);

                if ($queryResults->totalRows > 0) {
                    $response = array_merge($response, $this->parseResponse($queryResults));
                }
                $pageToken = $queryResults->pageToken ?? null;

            } while ($queryResults->jobComplete === false || $pageToken !== null);
        } catch (BigQueryApiException $e) {
            throw new BigQueryApiException($e->getMessage());
        }
        return $response;
    }

    /**
     * @throws BigQueryApiException|GoogleOauthException
     */
    public function createJob($query)
    {
        try {
            $response = $this->client->request('POST', $this->jobsPath, [
                'headers' => $this->getHeaders(),
                'body' => json_encode([
                    "jobReference" => [
                        "projectId" => $this->projectId
                    ],
                    "configuration" => [
                        "query" => [
                            "useLegacySql" => false,
                            "query" => $query
                        ]
                    ]
                ])
            ]);

        } catch (GuzzleException $exception) {
            throw new BigQueryApiException("Google BigQuery Jobs Error: " . $exception->getMessage());
        }

        return json_decode((string)$response->getBody(), true);
    }

    /**
     * @throws BigQueryApiException|GoogleOauthException
     */
    public function runJob($jobConfig, $pageToken = null)
    {
        $params = [];

        if ($pageToken) {
            $params['pageToken'] = $pageToken;
        }

        try {
            $response = $this->client->request('GET', $this->path
                . $jobConfig['jobReference']['projectId']
                . '/queries/' . $jobConfig['jobReference']['jobId'], [
                'headers' => $this->getHeaders(),
                'query' => $params
            ]);

        } catch (GuzzleException $exception) {
            throw new BigQueryApiException("Google BigQuery Queries Error. Result: " . $exception->getMessage());
        }

        return json_decode((string)$response->getBody());
    }

    public function parseResponse($response): array
    {
        $data = [];
        $fields = $response->schema->fields;
        $rows = $response->rows;

        foreach ($rows as $row) {
            $rowFields = [];
            foreach ($row->f as $index => $field) {
                $rowFields[$fields[$index]->name] = $field->v;
            }
            $data[] = $rowFields;
        }

        return $data;
    }

    /**
     * @throws BigQueryApiException|GoogleOauthException
     */
    public function getDataset($datasetId)
    {
        try {
            $response = $this->client->request('GET', $this->path . $this->projectId . '/datasets/' . $datasetId, [
                'headers' => $this->getHeaders(),
            ]);

        } catch (GuzzleException $exception) {
            throw new BigQueryApiException("BigQuery API Error. Result: " . $exception->getMessage());
        }

        return json_decode((string)$response->getBody(), true);
    }
}
