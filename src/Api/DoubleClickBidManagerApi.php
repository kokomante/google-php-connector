<?php

namespace Kokomante\GoogleConnector\Api;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Kokomante\GoogleConnector\Auth\GoogleOAuth2;
use Kokomante\GoogleConnector\Auth\GoogleScopes;
use Kokomante\GoogleConnector\Exceptions\DoubleClickBidManagerApiException;
use Kokomante\GoogleConnector\Exceptions\GoogleOauthException;

class DoubleClickBidManagerApi
{
    const DBM_API_URL = 'https://www.googleapis.com/doubleclickbidmanager/';
    const DEFAULT_VERSION = 'v1.1';

    private GoogleOAuth2 $googleOAuth2;
    private Client $client;
    private string $apiPath;
    private string $queryPath;

    public function __construct(GoogleOAuth2 $googleOAuth2, Client $client, $apiVersion)
    {
        $this->googleOAuth2 = $googleOAuth2;
        $this->client = $client;
        $this->apiPath = self::DBM_API_URL . $apiVersion . '/';
        $this->queryPath = $this->apiPath . 'query';
    }

    /**
     * @throws GoogleOauthException
     */
    private function getHeaders(): array
    {
        return [
            'Authorization' => 'Bearer ' . $this->googleOAuth2->getAccessToken()
        ];
    }

    /**
     * @throws DoubleClickBidManagerApiException
     */
    public static function fromConfig($config): DoubleClickBidManagerApi
    {
        self::checkConfig($config);

        $googleOAuth2 = new GoogleOAuth2(
            $config['clientId'],
            $config['clientSecret'],
            [GoogleScopes::DOUBLE_CLICK_BID_MANAGER_SCOPE],
            $config['refreshToken'] ?? null,
        );

        $apiVersion = $config['version'] ?? self::DEFAULT_VERSION;

        return new DoubleClickBidManagerApi($googleOAuth2, new Client(), $apiVersion);
    }

    /**
     * @throws DoubleClickBidManagerApiException
     */
    private static function checkConfig($config)
    {
        if (!is_array($config)) {
            throw new DoubleClickBidManagerApiException("Double Click Bid Manager config is not an array");
        }

        $configKeys = ['clientId', 'clientSecret', 'refreshToken'];

        foreach ($configKeys as $key) {
            if (!key_exists($key, $config) || !$config[$key]) {
                throw new DoubleClickBidManagerApiException($key . " not set in Double Click Bid Manager config");
            }
        }
    }

    /**
     * @throws DoubleClickBidManagerApiException
     * @throws GoogleOauthException
     */
    public function makeRequest(string $method, string $endpoint, $params = [])
    {
        $data['headers'] = $this->getHeaders();

        if ($method === 'POST') {
            $data['body'] = json_encode($params);
        }

        try {
            $response = $this->client->request($method, $endpoint, $data);
        } catch (GuzzleException $exception) {
            throw new DoubleClickBidManagerApiException("Double Click Bid Manager API Error: " . $exception->getMessage());
        }

        return json_decode((string)$response->getBody(), true);
    }

    /**
     * @throws DoubleClickBidManagerApiException|GoogleOauthException
     */
    public function createQuery(array $params)
    {
        return $this->makeRequest('POST', $this->queryPath, $params);
    }

    /**
     * @throws DoubleClickBidManagerApiException|GoogleOauthException
     */
    public function deleteQuery(int $queryId)
    {
        return $this->makeRequest('DELETE', $this->queryPath . '/' . $queryId);
    }

    /**
     * @throws DoubleClickBidManagerApiException|GoogleOauthException
     */
    public function getQueryMetadata(int $queryId): array
    {
        return $this->makeRequest('GET', $this->apiPath . 'query/' . $queryId);
    }

    /**
     * @throws DoubleClickBidManagerApiException|GoogleOauthException
     */
    public function getReport(int $queryId): array
    {
        $queryInstance = $this->getQueryMetadata($queryId);
        $csvContent = file_get_contents($queryInstance['metadata']['googleCloudStoragePathForLatestReport']);
        return $this->formatReportData($csvContent);
    }

    public function formatReportData($csvContent): array
    {
        $lineItems = str_getcsv($csvContent, "\n");
        $headers = str_getcsv(array_shift($lineItems));

        // switch headers to camel case
        $i = 0;
        foreach ($headers as $header) {
            $headers[$i] = $this->stringToCamelCase($header);
            $i++;
        }

        $queryMetadata = [];
        foreach ($lineItems as $lineItem) {
            $row = [];
            $lineArray = array_filter(str_getcsv($lineItem));

            if (count($lineArray) < 7) {
                continue;
            }

            foreach ($lineArray as $fieldName => $fieldValue) {
                $row[$headers[$fieldName]] = $fieldValue;
            }
            $row = array_filter($row);
            if (array_key_exists('date', $row)) {
                $row['date'] = Carbon::parse($row['date'])->format('Y-m-d');
            }
            $queryMetadata[] = $row;
        }

        return $queryMetadata;
    }

    public function stringToCamelCase($str, array $noStrip = []): string
    {
        // non-alpha and non-numeric characters become spaces
        $str = preg_replace('/[^a-z0-9' . implode("", $noStrip) . ']+/i', ' ', $str);

        // Lower all -> Upper first char of every word -> remove spaces -> Lower first char again
        return lcfirst(str_replace(" ", "", ucwords(strtolower($str))));
    }

    /**
     * @throws DoubleClickBidManagerApiException
     */
    public function generateParams($advertiserId, $groupBys = [], $metrics = [], $startDate = null, $endDate = null, $type = 'TYPE_GENERAL', $dataRange = 'CUSTOM_DATES'): array
    {
        $params = [
            "kind" => "doubleclickbidmanager#query",
            "metadata" => [
                "title" => ucwords(strtolower(str_replace('_', ' ', $type))) . " Query - " . Carbon::now(),
                "dataRange" => $dataRange,
                "format" => "CSV",
                "running" => true,
                "sendNotification" => false
            ],
            "params" => [
                "type" => $type,
                "groupBys" => $groupBys,
                "filters" => [
                    [
                        "type" => "FILTER_ADVERTISER",
                        "value" => $advertiserId
                    ],
                ],
                "metrics" => $metrics
            ],
            "schedule" => [
                "frequency" => 'ONE_TIME',
            ],
            "timezoneCode" => "Europe/Copenhagen"
        ];

        if($dataRange === 'CUSTOM_DATES'){
            if(is_null($startDate) || is_null($endDate)){
                throw new DoubleClickBidManagerApiException("Double Click Bid Manager Error: Custom dates requires startDate and endDate.");
            }
            $params['reportDataStartTimeMs'] = Carbon::parse($startDate, 'Europe/Copenhagen')->startOfDay()->getPreciseTimestamp(3);
            $params['reportDataEndTimeMs'] = Carbon::parse($endDate, 'Europe/Copenhagen')->startOfDay()->getPreciseTimestamp(3);
        }
        return $params;
    }
}