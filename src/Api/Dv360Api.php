<?php

namespace Kokomante\GoogleConnector\Api;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Kokomante\GoogleConnector\Auth\GoogleOAuth2;
use Kokomante\GoogleConnector\Auth\GoogleScopes;
use Kokomante\GoogleConnector\Exceptions\Dv360ApiException;
use Kokomante\GoogleConnector\Exceptions\GoogleOauthException;

class Dv360Api
{
    const DV360_API_URL = "https://displayvideo.googleapis.com/";
    const DEFAULT_VERSION = 'v1';

    private GoogleOAuth2 $googleOAuth2;
    private Client $client;
    private string $apiPath;
    private string $advertiserPath;

    public function __construct(GoogleOAuth2 $googleOAuth2, Client $client, $apiVersion)
    {
        $this->googleOAuth2 = $googleOAuth2;
        $this->client = $client;
        $this->apiPath = self::DV360_API_URL . $apiVersion . '/';
        $this->advertiserPath = $this->apiPath . 'advertisers/';
    }

    /**
     * @throws GoogleOauthException
     */
    private function getHeaders(): array
    {
        return [
            'Authorization' => 'Bearer ' . $this->googleOAuth2->getAccessToken()
        ];
    }

    /**
     * @throws Dv360ApiException
     */
    public static function fromConfig($config): Dv360Api
    {
        self::checkConfig($config);

        $googleOAuth2 = new GoogleOAuth2(
            $config['clientId'],
            $config['clientSecret'],
            [GoogleScopes::DV360_SCOPE],
            $config['refreshToken'] ?? null,
        );

        $apiVersion = $config['version'] ?? self::DEFAULT_VERSION;

        return new Dv360Api($googleOAuth2, new Client(), $apiVersion);
    }

    /**
     * @throws Dv360ApiException
     */
    private static function checkConfig($config)
    {
        if (!is_array($config)) {
            throw new Dv360ApiException("Dv360 config is not an array");
        }

        $configKeys = ['clientId', 'clientSecret', 'refreshToken'];

        foreach ($configKeys as $key) {
            if (!key_exists($key, $config) || !$config[$key]) {
                throw new Dv360ApiException($key . " not set in Dv360 config");
            }
        }
    }

    /**
     * @throws GoogleOauthException
     * @throws Dv360ApiException
     */
    public function makeRequest(string $method, string $endpoint, $params = [])
    {
        $data['headers'] = $this->getHeaders();

        if($method === 'POST'){
            $data['body'] = $params;
        }

        try {
            $response = $this->client->request($method, $endpoint, $data);
        } catch (GuzzleException $exception) {
            throw new Dv360ApiException("Dv360 API Error: " . $exception->getMessage());
        }

        return json_decode((string)$response->getBody(), true);
    }


    /**
     * @param int $advertiserId
     * @return mixed
     * @throws Dv360ApiException
     * @throws GoogleOauthException
     */
    public function getAdvertiser(int $advertiserId)
    {
        return $this->makeRequest('GET', $this->advertiserPath . $advertiserId);
    }

    /**
     * @param int $partnerId
     * @return mixed
     * @throws Dv360ApiException
     * @throws GoogleOauthException
     */
    public function getPartner(int $partnerId)
    {
        return $this->makeRequest('GET', $this->apiPath . 'partners/' . $partnerId);
    }

    /**
     * @param int $advertiserId
     * @return mixed
     * @throws Dv360ApiException
     * @throws GoogleOauthException
     */
    public function getCampaigns(int $advertiserId)
    {
        $campaigns = $this->makeRequest('GET', $this->advertiserPath . $advertiserId . '/campaigns');

        // Format status and dates
        foreach ($campaigns['campaigns'] as $index => $campaign) {
            $campaigns['campaigns'][$index]['entityStatus'] = str_replace('ENTITY_STATUS_', '', $campaign['entityStatus']);
            $campaigns['campaigns'][$index]['campaignFlight']['plannedDates']['startDate'] = Carbon::parse($campaign['campaignFlight']['plannedDates']['startDate']['year'] . '-' . $campaign['campaignFlight']['plannedDates']['startDate']['month'] . '-' . $campaign['campaignFlight']['plannedDates']['startDate']['day'])->format('Y-m-d');
            $campaigns['campaigns'][$index]['campaignFlight']['plannedDates']['endDate'] = Carbon::parse($campaign['campaignFlight']['plannedDates']['endDate']['year'] . '-' . $campaign['campaignFlight']['plannedDates']['endDate']['month'] . '-' . $campaign['campaignFlight']['plannedDates']['endDate']['day'])->format('Y-m-d');
        }

        return $campaigns;
    }

    /**
     * @param int $advertiserId
     * @return mixed
     * @throws Dv360ApiException
     * @throws GoogleOauthException
     */
    public function getLineItems(int $advertiserId)
    {
        $lineItems = $this->makeRequest('GET', $this->advertiserPath . $advertiserId . '/lineItems');

        // Format status
        foreach ($lineItems['lineItems'] as $index => $lineItem) {
            $lineItems['lineItems'][$index]['entityStatus'] = str_replace('ENTITY_STATUS_', '', $lineItem['entityStatus']);
            $lineItems['lineItems'][$index]['flight']['dateRange']['startDate'] = Carbon::parse($lineItem['flight']['dateRange']['startDate']['year'] . '-' . $lineItem['flight']['dateRange']['startDate']['month'] . '-' . $lineItem['flight']['dateRange']['startDate']['day'])->format('Y-m-d');
            $lineItems['lineItems'][$index]['flight']['dateRange']['endDate'] = Carbon::parse($lineItem['flight']['dateRange']['endDate']['year'] . '-' . $lineItem['flight']['dateRange']['endDate']['month'] . '-' . $lineItem['flight']['dateRange']['endDate']['day'])->format('Y-m-d');
        }

        return $lineItems;
    }

    /**
     * @param int $advertiserId
     * @return mixed
     * @throws Dv360ApiException
     * @throws GoogleOauthException
     */
    public function getInsertionOrders(int $advertiserId)
    {
        $insertionOrders = $this->makeRequest('GET', $this->advertiserPath . $advertiserId . '/insertionOrders');

        // Format status
        foreach ($insertionOrders['insertionOrders'] as $index => $insertionOrder) {
            $insertionOrders['insertionOrders'][$index]['entityStatus'] = str_replace('ENTITY_STATUS_', '', $insertionOrder['entityStatus']);
        }

        return $insertionOrders;
    }


    /**
     * @param int $advertiserId
     * @param int $creativeId
     * @return mixed
     * @throws Dv360ApiException
     * @throws GoogleOauthException
     */
    public function getCreative(int $advertiserId, int $creativeId)
    {
        return $this->makeRequest('GET', $this->advertiserPath . $advertiserId . '/creatives/' . $creativeId);
    }
}