<?php

namespace Kokomante\GoogleConnector\Auth;

class GoogleScopes
{
    /**
     * @var string the OAuth2 scopes for the APIs
     * @see https://developers.google.com/adwords/api/docs/guides/authentication#scope
     */
    const ADWORDS_API_SCOPE = 'https://www.googleapis.com/auth/adwords';
    const AD_MANAGER_API_SCOPE = 'https://www.googleapis.com/auth/dfp';
    const ANALYTICS_READ_ONLY_SCOPE = 'https://www.googleapis.com/auth/analytics.readonly';
    const BIGQUERY_READ_ONLY_SCOPE = "https://www.googleapis.com/auth/bigquery.readonly";
    const CALENDAR_READ_ONLY_SCOPE = 'https://www.googleapis.com/auth/calendar.readonly';
    const DOUBLE_CLICK_BID_MANAGER_SCOPE = 'https://www.googleapis.com/auth/doubleclickbidmanager';
    const DV360_SCOPE = 'https://www.googleapis.com/auth/display-video';
    const SPREADSHEETS_READ_ONLY_SCOPE = 'https://www.googleapis.com/auth/spreadsheets.readonly';

    const GOOGLE_PRODUCTS = [
        [
            'name' => 'AdWords API',
            'scope' => self::ADWORDS_API_SCOPE
        ],
        [
            'name' => 'Ad Manager API',
            'scope' => self::AD_MANAGER_API_SCOPE
        ],
        [
            'name' => 'Analytics ReadOnly API',
            'scope' => self::ANALYTICS_READ_ONLY_SCOPE
        ],
        [
            'name' => 'Big Query ReadOnly API',
            'scope' => self::BIGQUERY_READ_ONLY_SCOPE
        ],
        [
            'name' => 'Calendar ReadOnly API',
            'scope' => self::CALENDAR_READ_ONLY_SCOPE
        ],
        [
            'name' => 'Double Click Bid Manager API',
            'scope' => self::DOUBLE_CLICK_BID_MANAGER_SCOPE
        ],
        [
            'name' => 'Dv360 API',
            'scope' => self::DV360_SCOPE
        ],
        [
            'name' => 'Spreadsheets ReadOnly API',
            'scope' => self::SPREADSHEETS_READ_ONLY_SCOPE
        ],
    ];
}