<?php


namespace Kokomante\GoogleConnector\Auth;


use Google\Auth\CredentialsLoader;
use Google\Auth\OAuth2;
use Kokomante\GoogleConnector\Exceptions\GoogleOauthException;

class GoogleOAuth2
{
    /**
     * @var string the Google OAuth2 authorization URI for OAuth2 requests
     * @see https://developers.google.com/identity/protocols/OAuth2InstalledApp#formingtheurl
     */
    const AUTHORIZATION_URI = 'https://accounts.google.com/o/oauth2/v2/auth';

    /**
     * @var string the redirect URI for OAuth2 installed application flows
     * @see https://developers.google.com/identity/protocols/OAuth2InstalledApp#formingtheurl
     */
    const REDIRECT_URI = 'urn:ietf:wg:oauth:2.0:oob';

    private ?array $oauth2Config;
    private ?string $refreshToken;
    private OAuth2 $oauth2;

    public function __construct(string $clientId, string $clientSecret, array $scopes, ?string $refreshToken = null)
    {
        $this->refreshToken = $refreshToken;
        $this->setOauthConfig($clientId, $clientSecret, $scopes);
        $this->oauth2 = new OAuth2($this->getOauthConfig());
    }

    private function setOauthConfig($clientId, $clientSecret, array $scopes)
    {
        $this->oauth2Config = [
            'authorizationUri' => self::AUTHORIZATION_URI,
            'redirectUri' => self::REDIRECT_URI,
            'tokenCredentialUri' => CredentialsLoader::TOKEN_CREDENTIAL_URI,
            'clientId' => $clientId,
            'clientSecret' => $clientSecret,
            'scope' => implode(" ", $scopes)
        ];
    }

    public function getOauthConfig(): ?array
    {
        return $this->oauth2Config;
    }

    public function setOauth2(OAuth2 $oauth2) {
        $this->oauth2 = $oauth2;
    }

    /**
     * @throws GoogleOauthException
     */
    public function getAccessToken()
    {
        if(!$this->refreshToken) {
            $url = $this->oauth2->buildFullAuthorizationUri();
            throw new GoogleOauthException('Refresh Token is not set. 
            Log into the Google account you use for your scope and visit the following URL: '.$url." Use the code generated
            in the method GoogleOAuth2::generateRefreshToken to obtain a valid refresh token");
        }
        $this->oauth2->setRefreshToken($this->refreshToken);
        $this->oauth2->setGrantType('refresh_token');
        return $this->oauth2->fetchAuthToken()['access_token'];
    }
}