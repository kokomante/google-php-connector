<?php


namespace Kokomante\GoogleConnector\Console;


use Google\Auth\CredentialsLoader;
use Google\Auth\OAuth2;
use Kokomante\GoogleConnector\Auth\GoogleScopes;

class GenerateRefreshTokenCommand
{
    /**
     * @var string the Google OAuth2 authorization URI for OAuth2 requests
     * @see https://developers.google.com/identity/protocols/OAuth2InstalledApp#formingtheurl
     */
    const AUTHORIZATION_URI = 'https://accounts.google.com/o/oauth2/v2/auth';

    /**
     * @var string the redirect URI for OAuth2 installed application flows
     * @see https://developers.google.com/identity/protocols/OAuth2InstalledApp#formingtheurl
     */
    const REDIRECT_URI = 'urn:ietf:wg:oauth:2.0:oob';

    /**
     * Execute the console command.
     */
    public static function run(): int
    {
        $stdin = fopen('php://stdin', 'r');

        print 'Enter your OAuth2 client ID here: ';
        $clientId = trim(fgets($stdin));
        system('clear');

        print 'Enter your OAuth2 client secret here: ';
        $clientSecret = trim(fgets($stdin));
        system('clear');

        $endApisSelection = false;
        $selectedApis = [];
        do {
            system('clear');
            print 'Select API\'s: ' . self::generateSelectedApisString($selectedApis);

            $userInput = trim(fgets($stdin));

            if ($userInput === 'q') {
                $endApisSelection = true;
            } else {
                if (
                    !is_numeric($userInput)
                    || !($userInput >= 0 && $userInput <= (count(GoogleScopes::GOOGLE_PRODUCTS) - 1))
                ) {
                    continue;
                }

                $userInput = (int)$userInput;
                if (!in_array($userInput, $selectedApis)) {
                    $selectedApis[] = $userInput;
                } else {
                    if (($key = array_search($userInput, $selectedApis)) !== false) {
                        unset($selectedApis[$key]);
                    }
                }
            }

        } while (!$endApisSelection);

        $currentApisString = $currentApisScopes = '';
        if (!empty($selectedApis)) {
            foreach ($selectedApis as $index => $api) {
                $currentApisString .= GoogleScopes::GOOGLE_PRODUCTS[$api]['name'];
                $currentApisScopes .= GoogleScopes::GOOGLE_PRODUCTS[$api]['scope'];
                if ($index + 1 !== count($selectedApis)) {
                    $currentApisString .= ', ';
                    $currentApisScopes .= ' ';
                }
            }
        }

        system('clear');
        printf(
            '[OPTIONAL] enter any additional OAuth2 scopes as a space delimited string here (the %s scopes are already included): ',
            $currentApisString
        );

        $scopes = $currentApisScopes . ' ' . trim(fgets($stdin));

        $oauth2 = new OAuth2(
            [
                'authorizationUri' => self::AUTHORIZATION_URI,
                'redirectUri' => self::REDIRECT_URI,
                'tokenCredentialUri' => CredentialsLoader::TOKEN_CREDENTIAL_URI,
                'clientId' => $clientId,
                'clientSecret' => $clientSecret,
                'scope' => $scopes
            ]
        );


        printf(
            "Log into the Google account you use  and visit the following"
            . " URL:\n%s\n\n",
            $oauth2->buildFullAuthorizationUri()
        );
        print 'After approving the application, enter the authorization code '
            . 'here: ';
        $code = trim(fgets($stdin));
        fclose($stdin);
        print "\n";

        $oauth2->setCode($code);
        $authToken = $oauth2->fetchAuthToken();

        printf("Your refresh token is: %s\n\n", $authToken['refresh_token']);

        return 0;
    }

    private static function generateSelectedApisString($selectedApis): string
    {
        $scopesString = '' . PHP_EOL;
        foreach (GoogleScopes::GOOGLE_PRODUCTS as $index => $product) {
            if (in_array($index, $selectedApis)) {
                $scopesString .= '[*] ';
            } else {
                $scopesString .= '[ ] ';
            }
            $scopesString .= '[' . $index . '] ' . $product['name'] . PHP_EOL;
        }
        $scopesString .= '[q] End selection' . PHP_EOL;
        return $scopesString;
    }
}